import RPi.GPIO as GPIO
from time import sleep
import rnd

from consts import(
    FULL_CYCLE_DELAY,
    HOME_TO_A_SPEED,
    HOME_MICROSW,
    PUL,
    DIR,
    DC_MOTOR,
    STOP_OPERATION_BUTTON,
    STEP_DISTANCE,
    MICROSTEP_MULTIPLIER,
    STEPS_IN_CYCLE,
    DC_MOTOR_FULL_CYCLE_TIME,
    HOME_MICROSW,
    STEP_DISTANCE,
    HOME_TO_A_SPEED,
    ARCH_LEN_PER_STEP,
    DEBUG,
)

# direction constants
FORWARD = True
BACKWARDS = False

class Rpi():
    ret_stop_operation = 0

    def __init__(self):
        # setting up the GPIO modes and more
        rnd.file_parser()
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(PUL, GPIO.OUT)
        GPIO.setup(DIR, GPIO.OUT)
        GPIO.setup(DC_MOTOR, GPIO.OUT)
        GPIO.setup(HOME_MICROSW, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(STOP_OPERATION_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    # TODO: add formula to transfer rpm to delay time between each step
    def one_step(self, delay):
        """Moves the stepper motor one step"""
        GPIO.output(PUL, GPIO.HIGH)
        sleep(delay)
        GPIO.output(PUL, GPIO.LOW)
        sleep(delay)

    def change_direction(slef, dir):
        """Changes the direction of the stepper motor"""
        if dir:
            GPIO.output(DIR, GPIO.HIGH)
        else:
            GPIO.output(DIR, GPIO.LOW)

    def one_cycle(self):
        """Spins the stepper motor one turn"""
        for step in range(STEPS_IN_CYCLE * MICROSTEP_MULTIPLIER):
            # I chose not to call the one_step function since
            # rapid function calls seem to take a little time
            GPIO.output(PUL, GPIO.HIGH)
            sleep(FULL_CYCLE_DELAY / MICROSTEP_MULTIPLIER)
            GPIO.output(PUL, GPIO.LOW)
            sleep(FULL_CYCLE_DELAY / MICROSTEP_MULTIPLIER)

    def dc_motor_on(self):
        """Turn on the DC motor"""
        GPIO.output(DC_MOTOR, GPIO.HIGH)
    
    def dc_motor_off(self):
        """Turn off the DC motor"""
        GPIO.output(DC_MOTOR, GPIO.LOW)

    def dc_motor_one_cycle(self):
        """Spins the DC motor one turn"""
        self.dc_motor_on()
        sleep(DC_MOTOR_FULL_CYCLE_TIME)
        self.dc_motor_off()

    def dc_motor_cycles_preset(self):
        """Spins the DC motor a preset number of times 
        before the operation"""
        for cycle in range(rnd.dc_motor_cycle):
            self.dc_motor_one_cycle()

    def get_number_of_steps_from_distance(self, distance):
        """Returns the number of steps in a certain distance in mm"""
        return int(distance / STEP_DISTANCE)

    # TODO: add formula
    def get_delay_from_speed(self, speed):
        """Returns the delay value needed for the sleep() function 
        in order to spin the stepper motor at a certain speed (in RPM)
        """
        # sleep_val_per_step = (1 / STEPS_IN_CYCLE) * (60 / 2)
        # return sleep_val_per_step / speed

        return (ARCH_LEN_PER_STEP / speed) / 2

    def operation_stopped_callback(self, channel):
        """A callback function for the stop-button interrupt"""
        self.dc_motor_off()
        print("Operation stopped")
        inp = input("Do you want to continue? [N/y]: ")
        if inp == 'y':
            return 0
        else:
            self.homing()
            self.ret_stop_operation = 1
            return 1
            # TODO: figure out how to make it stop everything and restart main
        
    def homing(self):
        """Moves the cart to home position"""
        self.change_direction(BACKWARDS)
        while not GPIO.input(HOME_MICROSW) == GPIO.HIGH:
            self.one_step(HOME_TO_A_SPEED)

    def move_to_next_section(self, next_section):
        """Moves the cart along the next section"""
        # loading the stop-button operation interrupt
        GPIO.add_event_detect(STOP_OPERATION_BUTTON, GPIO.RISING, 
            callback=self.operation_stopped_callback, bouncetime=100)

        self.change_direction(FORWARD)

        self.dc_motor_on()

        if next_section == 'a':
            distance = rnd.pos_b - rnd.pos_a
            speed = self.get_delay_from_speed(rnd.speed_a)
        elif next_section == 'b':
            distance = rnd.pos_c - rnd.pos_b
            speed = self.get_delay_from_speed(rnd.speed_b)
        elif next_section == 'c':
            distance = rnd.pos_d - rnd.pos_c
            speed = self.get_delay_from_speed(rnd.speed_c)
        # the section that's between the home and point A
        elif next_section == '0':
            distance = rnd.pos_a
            speed = HOME_TO_A_SPEED

        if DEBUG:
                print(next_section)

        # self.get_number_of_steps_from_distance(distance)
        for step in range(self.get_number_of_steps_from_distance(distance)):
            if self.ret_stop_operation:
                break
            self.one_step(speed)

            # printing the step number for debugging
            if DEBUG:
                print(step)

        self.dc_motor_off()

        # unloding the stop-button operation interrupt
        GPIO.remove_event_detect(STOP_OPERATION_BUTTON)
        return self.ret_stop_operation
