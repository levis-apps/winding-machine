import os
from consts import(DEBUG)

path_to_file = "rnd.txt"

speed_a = 0
speed_b = 0
speed_c = 0
pos_a = 0
pos_b = 0
pos_c = 0
pos_d = 0
dc_motor_cycle = 0

def change_specific_line(line, line_inp):
    """Changes a specific line in a file"""
    with open(path_to_file, 'r') as file:
        # read a list of lines into data
        data = file.readlines()

    # changing the line number (line-1)
    data[line] = line_inp

    # writing everything back
    with open(path_to_file, 'w') as file:
        file.writelines(data)

def file_parser():
    """Reads the file and loads the globals variables with it's data"""
    global speed_a
    global speed_b
    global speed_c
    global pos_a
    global pos_b
    global pos_c
    global pos_d
    global dc_motor_cycle

    if not os.path.exists(path_to_file):
        # initializing the file
        default_data = ["0\n", "0\n", "0\n",
                        "18\n", "77\n", "1123\n", "1145\n", "1\n"]
        with open(path_to_file, 'w+') as file:
            file.writelines(default_data)

    with open(path_to_file, 'r') as file:
        # read a list of lines into data
        data = file.readlines()

    speed_a = float(data[0])
    speed_b = float(data[1])
    speed_c = float(data[2])
    pos_a = int(data[3])
    pos_b = int(data[4])
    pos_c = int(data[5])
    pos_d = int(data[6])
    dc_motor_cycle = int(data[7])

def show_current_config():
    """Prints out the current configuration"""
    file_parser()
    print(
        "\n"
        "The speed at section A: {}\n"
        "The speed at section B: {}\n"
        "The speed at section C: {}\n"
        "The position of point A: {}\n"
        "The position of point B: {}\n"
        "The position of point C: {}\n"
        "The position of point D: {}\n"
        "The number of DC motor cycles before operation: {}\n"
        .format(speed_a, speed_b, speed_c, pos_a, pos_b, pos_c, pos_d, dc_motor_cycle)
    )

def set_cart_speed():
    """A place to let the user set the cart's speed in each section"""
    print("Select the section for which you would like to set the speed\n")
    sec_inp = input("(A-C): ").lower()
    line_number = 0
    if sec_inp == 'a':
        line_number = 0
    elif sec_inp == 'b':
        line_number = 1
    elif sec_inp == 'c':
        line_number = 2
    else:
        print("Error: Invalid input\n")
        return 1

    speed_inp = float(input("Enter speed: "))
    change_specific_line(line_number, str(speed_inp) + "\n")
    return 0

def set_point_pos():
    """A place for the user to set the position of each point"""
    print("Select the point for which you would like to set the position\n")
    point_inp = input("(A-D): ").lower()
    line_number = 3
    if point_inp == 'a':
        line_number = 3
    elif point_inp == 'b':
        line_number = 4
    elif point_inp == 'c':
        line_number = 5
    elif point_inp == 'd':
        line_number = 6
    else:
        print("Error: Invalid input\n")
        return 1

    pos_inp = int(input("Enter the position: "))
    change_specific_line(line_number, str(pos_inp) + "\n")
    return 0

def check_password():
    """Checks if the R&D password is correct"""
    if not os.path.exists("pass.txt"):
        password = input(
            "It's your first time here, please enter a password: ")
        with open("pass.txt", 'w+') as file:
            file.write(password)
        return True
    else:
        with open("pass.txt", 'r') as file:
            password = file.read()

        inp_password = input("Enter password: ")
        return inp_password == password


def change_password():
    """A place to change the R&D password"""
    new_password = input("Enter new password: ")
    with open("pass.txt", 'w+') as file:
        file.write(new_password)

def set_dc_motor_cycles():
    """A place to set the number of cycles to spin the DC motor 
    at the beginning of the operation"""
    print(
        "Insert the number of cycles to spin the DC motor\n"
        "at the beginning of the operation"
        )
    cycles = int(input("(1,2,...): "))
    line_number = 7
    change_specific_line(line_number, str(cycles) + "\n")

# will be called by the main flow
def change_rnd():
    """Main R&D flow"""
    if not check_password():
        print("Wrong password!\n")
        return 1

    while True:
        file_parser()

        os.system("clear")
        print("~ This is the R&D mode ~\nPick things you want to set:\n")
        print(
            "1) Cart speed in each section (A-C)\n"
            "2) Points position (A-D)\n"
            "3) Amount of cycles at the beginning\n"
            "4) Change R&D manu password\n"
            "5) Show current configuration\n"
            "6) Exit R&D\n"
        )

        inp = input("Enter a number: ")
        if inp == '1':
            set_cart_speed()
        elif inp == '2':
            set_point_pos()
        elif inp == '3':
            set_dc_motor_cycles()
        elif inp == '4':
            change_password()
        elif inp == '5':
            pass
        elif inp == '6':
            break
        else:
            print("Error: Invalid input\n")
            continue

        show_current_config()

        inp2 = input("Press enter to continue, press r to stay in R&D: ")
        if inp2 == 'r':
            continue
        break
    return 0
