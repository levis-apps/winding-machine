import os
from sys import stdout
from time import sleep
import rpi
import rnd
from consts import(
    DEBUG,
    FULL_CYCLE_DELAY
)

def five_sec_delay():
    for sec in range(5):
        # displaying countdown
        stdout.write("\r%d" % (5 - sec))
        sleep(1)
        stdout.flush()

def main_menu():
    while True:
            print("~ Main Menu ~")
            print(
                "1) Start operation\n"
                "2) R&D mode\n"
            )
            menu_inp = input("Enter a number: ")
            # enter 2 for R&D mode
            if menu_inp == '2':
                os.system("clear")
                rnd.change_rnd()
                continue
            # enter 1 for beginning the operation
            elif menu_inp == '1':
                os.system("clear")
                break
            else:
                print("Error: Invalid input\n")
                continue

# homing and waiting to start
def begin_operation_and_rehoming(homing_func):
    while True:
                homing_func()
                print("Ready to install")
                ready = input(
                    "Press enter when you are ready, press h and then enter to rehome: ").lower()
                if ready == 'h':
                    continue
                else:
                    break

# do x amount of turns until ready
def full_cycles_til_ready(dc_cycle):
        while True:
            print("Ready?")
            inp = input("(n/Y): ").lower()
            if inp == 'n':
                dc_cycle()
                continue
            else:
                break

def main():
    while True:
        os.system("clear")
        main_menu()
        # setup the rpi module
        pi = rpi.Rpi()

        begin_operation_and_rehoming(pi.homing)
        # moving the cart from home to the point A
        pi.move_to_next_section('0')

        five_sec_delay()
        
        # spins the DC motor for a preset number of cycles
        pi.dc_motor_cycles_preset()
        full_cycles_til_ready(pi.dc_motor_one_cycle)

        # move between sections
        # main rpi operation
        print("Section A")
        if pi.move_to_next_section('a'):
            continue
        print("Section B")
        if pi.move_to_next_section('b'):
            continue
        print("Section C")
        if pi.move_to_next_section('c'):
            continue
        print("Finished")

        # halting after finishing
        input("Press enter to continue\n")
        continue

if __name__ == "__main__":
    main()
