#### DEBUG ####
DEBUG = False # if True than the code will output some debug prints
#### DEBUG ####

#### GPIO PINS ####
PUL = 17 # PU-
DIR = 27 # DR-
DC_MOTOR = 4 # relay
STOP_OPERATION_BUTTON = 13
HOME_MICROSW = 22 # a microswitch at the home position
#### GPIO PINS ####

#### MAIN CONSTANTS ####
# TODO: measure/calculate the amount of steps in a full cycle
# I think I found the exact motor on the internet and
# every step is 1.8deg which means that there are 200 steps in a cycle or 400 half steps
STEPS_IN_CYCLE = 200
# the divider that decides the amount of microsteps in a cycle (400 steps = 2)
MICROSTEP_MULTIPLIER = 1 # default is 1, chnages with the switches on the driver
# TODO: measure/calculate the amount of time it takes to finish a cycle
# TODO: measure the distance of a full cycle
FULL_CYCLE_DISTANCE = 140 # mm
# at 200 steps/cycle
FULL_CYCLE_TIME = 4 # sec
# at 4 volts
DC_MOTOR_FULL_CYCLE_TIME = 2.4 # sec
ARCH_LEN_PER_STEP = 0.628

#### MAIN CONSTANTS ####

#### CALCULATIONS ####
# TODO: measure the distance of a step
# the distance the cart travels during a single step
# currently, not very accurate
STEP_DISTANCE = FULL_CYCLE_DISTANCE / (STEPS_IN_CYCLE * MICROSTEP_MULTIPLIER) # mm
# the speed between the home point to A
HOME_TO_A_SPEED = 0.005
# full cycle time to RPM
FULL_CYCLE_SPEED = (100 / FULL_CYCLE_TIME) * 60 # RPM
# the parameter to pass the sleep() function in the one_cycle() function
# in order to set the full cycle time to FULL_CYCLE_TIME
FULL_CYCLE_DELAY = (FULL_CYCLE_TIME / STEPS_IN_CYCLE) / 2 # sec | 0.01 = 4 sec
#### CALCULATIONS ####
