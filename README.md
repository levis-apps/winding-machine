# RPi Winding Machine
The full code for the winding machine.
Developed by Dvir Levy

## Description
This Python code will run on a Raspberry Pi and control a machine which will automatically wind a coil over a catheter

## Requierements
- [ ] Raspberry Pi
- [ ] Python 3

## Installation
### Clone
Clone this repository to the Raspberry Pi's home directory (~/):
```
git clone https://gitlab.com/levis-apps/winding-machine.git
```
### Alias
For ease of use create an alias in the `.bashrc` file:
```
echo 'alias start="python ~/winding-machine/main.py"' >> ~/.bashrc
``` 

## Usage
The best way to run this code is by entering `start`
or by running `main.py` using `python main.py` inside the directory
